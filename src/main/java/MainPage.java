import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "MainPage", urlPatterns={"/Servlet"})
public class MainPage extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,
            NullPointerException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String lname = request.getParameter("lname");
        String profession = request.getParameter("profession");
        String foodType = request.getParameter("foodType");
        String bodyPart = request.getParameter("bodyPart");
        String threat = request.getParameter("threat");
        String[] tags = request.getParameterValues("tags");
        String hometown = request.getParameter("hometown");
        String cheap = request.getParameter("cheap");
        String ingVerb = request.getParameter("ingVerb");
        String verb1 = request.getParameter("verb1");
        String greek = request.getParameter("greek");
        String tv = request.getParameter("tv");
        String level = request.getParameter("level");
        String oddNum = request.getParameter("oddNum");
        String smallNum = request.getParameter("smallNum");

        out.println("<h1><b>RESTRICTED TO PERSONNEL WITH SECURITY CLEARANCE " + level + "AND ABOVE" +
                " BY ORDER OF 05 COMMAND</b></h1>");
        out.println("<p><b>Item #:</b> SCP-" + oddNum + "-J</p>");
        out.println("<p><b>Object Class:</b> " + threat + "</p>");
        out.println("<p><b>Special Containment Procedures:</b> SCP-" + oddNum + "-J is to be kept in a " + foodType +
                "-lined containment chamber located in Site-Name "+ hometown + ", where it is to be guarded at all " +
                "times by no less than " + smallNum + " " + profession + " armed with " + cheap + ".</p>");
        out.println("<p>In the event that SCP-" + oddNum + "-J ever begins " + ingVerb + " its " + bodyPart +
                ", Doctor " + lname + " is to " + verb1 + " SCP-" + oddNum + "-J until it ceases its behavior. In" +
                "the event of a containment breach, Mobile Task Force " + greek + "-7 ('" + tv + "') is to be" +
                "dispatched to SCP-" + oddNum + "-J's last known location.</p>");
        out.println("<p><b>Unconfirmed Anomalous Properties:</b></p>");
        try {
        out.println("<ul>");
            for (String list : tags) {
                out.println("<li>" + list + "</li>");
            }
        out.print("</ul>");
        } catch (NullPointerException e) {
            out.println("<ul><li>None recorded.</li></ul>");
        }
        out.println("<p><b>ERROR:</b> Further contents unavailable for retrieval.</p>" +
                "<p>Please contact help desk for assistance.</p>");
        out.println("</body></html>");

        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {

        response.setContentType("text/html");
        String lname = request.getParameter("lname");

        if ((lname == null) || (lname.equals(""))) {
            throw new IllegalArgumentException("'LAST NAME' was left blank.");
        }

    }
}