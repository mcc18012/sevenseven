<%--
  Created by IntelliJ IDEA.
  User: McCleve Family
  Date: 12/3/2020
  Time: 8:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <style>
        body {
            text-align: center;
            background-color: #131;
            background-image: radial-gradient(ellipse 1000% 100% at 50% 90%, transparent, #121);
            background-position: center;
            display: block;
            box-shadow: inset 0 0 10em 1em rgba(0,0,0,0.5);
            overflow: hidden;
        }
        body blockquote {
            background-color: black;
            border: double 3px #80FF80;
        }
        scanline{
            margin-top: -40%;
            width: 100%;
            height: 60px;
            position: relative;
            pointer-events: none;
            /* Safari 4.0 - 8.0 */
            -webkit-animation: scan 12s linear 0s infinite;
            animation: scan 12s linear 0s infinite;
            background: linear-gradient(to bottom, rgba(56, 112, 82,0), rgba(56, 112, 82,0.1)) !important;
        }

        h1 {
            text-decoration: underline;
            color: rgba(128,255,128,0.8);
            padding-top: 5%;
            font-family: monospace;
        }
        @keyframes scan{
            from{ transform: translateY(-10%);}
            to{  transform: translateY(5000%);} /* Same as above.*/
        }
        blockquote {
            background-color: black;
            border: double 3px #80FF80;
            margin: 0.5em 2em 0.5em 2em;
            padding: 0 1em;
            display: block;
        }
        p {
            color: rgba(128,255,128,0.8);
            padding-left: 2em;
            padding-top:0.5em;
            font-family: monospace;
            font-size: 1.2em;
        }
    </style>
    <h1><span>TERMINAL #009</span></h1>
        <title>Foundation Database</title>
</head>
<body>
<p>---------</p><p>Welcome, User</p><p>---------</p>
<blockquote>
<p>To get to the html page click <a href="${pageContext.request.contextPath}/MadLib.html">here</a></p>
</blockquote>
</body>
</html>